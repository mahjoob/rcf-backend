<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Check Is Role in databese  If Not Create Role for Seeder
        $roleInDatabase = Role::where('name' , config('permission.default_roles')[0]);
        if($roleInDatabase->count() < 1){
            foreach (config('permission.default_roles') as $role){
                Role::create([
                    'name' => $role
                ]);
            }
        }

        // Check Is Permission in databese If Not Create Permission for Seeder
        $permissionInDatabase = Permission::where('name' , config('permission.default_permission')[0]);
        if($roleInDatabase->count() < 1){
            foreach (config('permission.default_permissions') as $permission){
                Permission::create([
                    'name' => $permission
                ]);
            }
        }
    }
}
