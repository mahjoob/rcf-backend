<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Answer;
use Faker\Generator as Faker;

$factory->define(Answer::class, function (Faker $faker) {
    return [
        'content' => $faker->realText(),
        'user_id' => \factory(\App\User::class)->create()->id,
        'thread_id' => factory(\App\Thread::class)->create()->id
    ];
});
