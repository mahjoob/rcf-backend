<?php

namespace App\Providers;

use App\Answer;
use App\Thread;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Implicitly grant "Super Admin" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('Super Admin') ? true : null;
        });

        //Check is user equal with user that write thread
        Gate::define('user-thread', function (User $user, Thread $thread) {
            return $user->id === $thread->user_id;
        });

        //Check is user equal with user that write answer
        Gate::define('user-answer', function (User $user, Answer $answer) {
            return $user->id === $answer->user_id;
        });
    }
}
