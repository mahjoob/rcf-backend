<?php

namespace Tests\Feature\API\v1\Channel;


use App\Channel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ChannelTest extends TestCase
{
    use RefreshDatabase;

    public function registerRoleAndPermissions()
    {
        $roleInDatabase = Role::where('name',config('permission.default_roles')[0]);
        if($roleInDatabase->count() < 1){
            foreach (config('permission.default_roles') as $role){
                Role::create([
                    'name' => $role
                ]);
            }
        }

        $permissionInDatabase = Permission::where('name',config('permission.default_permissions')[0]);
        if ($permissionInDatabase->count() < 1){
            foreach (config('permission.default_permissions') as $permission){
                Permission::create([
                    'name' => $permission
                ]);
            }
        }
    }

    /**
     * Test All Channel be Accessible
     */
    public function test_all_channels_list_should_be_accessible()
    {
        $response = $this->get(route('channel.all'));
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test Validation Channel
     */
    public function test_create_channel_should_be_validated()
    {
        $this->registerRoleAndPermissions();

        $user = factory(User::class)->create();
        Sanctum::actingAs($user);
        $user->givePermissionTo('channel management');

        $response = $this->postJson(route('channel.create'), []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test Creating Channel
     */
    public function test_create_new_channel()
    {
        $this->registerRoleAndPermissions();

        $user = factory(User::class)->create();
        Sanctum::actingAs($user);
        $user->givePermissionTo('channel management');

        $response = $this->postJson(route('channel.create'), [
            'name' => 'Laravel'
        ]);

        $response->assertStatus(Response::HTTP_CREATED);
    }


    /**
     * Test Validation Channel
     */
    public function test_channel_update_should_be_validated()
    {
         $this->registerRoleAndPermissions();

        $user = factory(User::class)->create();
        Sanctum::actingAs($user);
        $user->givePermissionTo('channel management');

        $response = $this->json('PUT',route('channel.update'), []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test Update Channel
     */
    public function test_channel_update()
    {
       $this->registerRoleAndPermissions();

       $user = factory(User::class)->create();
       Sanctum::actingAs($user);
       $user->givePermissionTo('channel management');

      $channel = factory(Channel::class)->create([
          'name' => 'laravel'
      ]);

        $response = $this->json('PUT',route('channel.update'), [
            'id' => $channel->id,
            'name' => 'Vuejs'
        ]);

        $updatedChannel = Channel::find($channel->id);

        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals('Vuejs', $updatedChannel->name);
    }

    /**
     * Test Validation Channel For Delete
     */
    public function test_channel_delete_should_be_validated()
    {
        $this->registerRoleAndPermissions();

        $user = factory(User::class)->create();
        Sanctum::actingAs($user);
        $user->givePermissionTo('channel management');

        $response = $this->json('DELETE',route('channel.delete'), []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Test Delete Channel
     */
    public function test_channel_delete()
    {
         $this->registerRoleAndPermissions();

        $user = factory(User::class)->create();
        Sanctum::actingAs($user);
        $user->givePermissionTo('channel management');

        $channel = factory(Channel::class)->create();

        $response = $this->json('DELETE',route('channel.delete'), [
            'id' => $channel->id]);

        $response->assertStatus(Response::HTTP_OK);

        $this->assertTrue(Channel::where('id',$channel->id)->count()=== 0);
    }
}
